@extends('layouts.app')

@section('content')
<div class="container">
    {{ Form::open(array('url' => '/writeLetter', 'class' => 'SentForm')) }}
        <div class="form-group">
            {{ Form::label('email', 'Получатель') }}
            {{ Form::text('email', '', ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('subject', 'Тема письма') }}
            {{ Form::text('subject', '', ['class' => 'form-control']) }}
        </div>
        <div class="form-group">
            {{ Form::label('text', 'Текс пиьма') }}
            {{ Form::textarea('text',null,['class'=>'form-control', "cols" => '20', 'rows' => '5']) }}
        </div>
        <div class="text-right">
            <button type="submit" class="btn btn-primary">Отправить</button>
            <a href="{{ url('/') }}" class="btn btn-primary">Отмена</a>
        </div>
    {{ Form::close() }}
</div>
@endsection