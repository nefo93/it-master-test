@extends('layouts.app')

@section('content')

<div class="container d-flex mt-3">
    <div class="w-25 mr-3 list-group">
        <a class="list-group-item <?php if(Request::url() == url('/')) echo 'active'; ?>" href="{{ url('/') }}">Входящие</a>
        <a class="list-group-item {!! (Request::url() == url('/sent'))? 'active' : '' !!}" href="{{ url('/sent') }}">Отправленные</a>
    </div>
    @if($letter)
    {{ Form::open(array('url' => '/delete', 'method' => 'post', 'class' => 'w-100')) }}

    <table class="table table-striped">
        <thead>
            <tr>
                <th><input type="checkbox" id="all"></th>
                <th>Отправитель</th>
                <th>Тема письма</th>
                <th>Дата</th>
            </tr>
        </thead>
        <tbody>
            @for($i = 0; $i < count($letter); $i++)
            <tr>
                <td>{!! Form::checkbox('id[]', $letter[$i]['id'], false, ['class' => 'check']) !!}</td>
                <td>{{ $letter[$i]['who'] }}</td>
                <td>{{ $letter[$i]['subject'] }}</td>
                <td>{{ $letter[$i]['date'] }}</td>
            </tr>
            @endfor
        </tbody>
    </table>
        <input type="text" value="{{ (Request::url() == url('/sent'))? true : false }}" class="d-none" name="boole">
        <input type="submit" class="btn btn-danger" value="Удалить">
    {{ Form::close() }}
    @else
    <h1>Сообщений нет</h1>
    @endif
</div>
@endsection
