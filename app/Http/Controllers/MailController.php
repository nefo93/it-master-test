<?php

namespace App\Http\Controllers;

use App\Services\MailServices;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Break_;


class MailController extends Controller
{
    protected $letter;

    public function __construct() {
        $this->middleware('auth');
        $this->letter = new MailServices('testilya993@gmail.com', '031293ilya031293');
    }

    public function index() {
        $mas = $this->letter->getLetter();
        return view('home', ['letter' => $mas]);
    }

    public function sent() {
        $mas = $this->letter->getSent();
        return view('home', ['letter' => $mas]);
    }

    public function write() {
        return view('write');
    }

    public function writeLetter(Request $request) {
        $this->letter->writeMessage($request->email, $request->subject, $request->text);
        return redirect('/sent');
    }

    public function deleteLetters(Request $request) {
        for($i = 0; $i < count($request->id); $i++) {
            if($request->boole) {
                $this->letter->deleteSent($request->id[$i]*1);
            } else {
                $this->letter->deleteIncoming($request->id[$i]*1);
            }
        }
        if($request->boole) {
            return redirect('/sent');
        }
        return redirect('/');
    }
}
